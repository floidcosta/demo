## Overview:
This project demonstrates the POC on REST services for fab retail. The project comprises of implementing spring boot and testing it through MockmVC.

## Database Design:
the tables are: 
1)PRODUCT : product details identified by SKU. 
## Technologies Used:
Java 11 
Spring MVC with Spring Boot (Version: 2.4.2) 
Maven (Version: 3.6.3) 
H2

## Dependencies
Spring Boot  
Lombok  
H2
Spring Web
MockMvc
jpa

## Running the Application
To run a Spring Boot application on your local machine. 
Select the main method FabRestApplication.java present in the com.fab.demo package. 
Right Click on the file and Run. 
Open the browser http://localhost:8080/swagger-ui.html