package com.fab.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FabRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(FabRestApplication.class, args);
	}

}
