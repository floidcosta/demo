package com.fab.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fab.demo.dao.FabRepository;
import com.fab.demo.model.Product;

@RestController
public class FabController {
	
	@Autowired
	FabRepository fabRepo; 
	
	@GetMapping("/getProduct")
	public List<Product> getProduct() {
		List<Product> ls= fabRepo.findAll();
		return ls;		
		
	}
	
	@GetMapping("/getProduct/{sku}")
	public Optional<Product> getProduct(@PathVariable("sku") Integer sku) {
		Optional<Product> ls= fabRepo.findById(sku);
		return ls;		
		
	}
	  /**
	   * @param artifactInfo saveProduct
	   * @return ResponseEntity responseEntity
	   */
	  @PostMapping("/saveProduct")
	  public ResponseEntity<HttpStatus> saveProduct(
	      @RequestBody final Product product) {
		  fabRepo.save(product);
	    return ResponseEntity.ok(HttpStatus.OK);
	  }
	


}
