package com.fab.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fab.demo.model.Product;

public interface FabRepository extends JpaRepository<Product, Integer> {

}
